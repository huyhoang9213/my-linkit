﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Linkit.Domain
{

    public partial class Asset
    {
        public int AssetId { get; set; }
        public int? AssetTypeId { get; set; }
        public int? AssetCategoryId { get; set; }
        public int? AssetFileTypeId { get; set; }
        public string Topic { get; set; }
        public string Description { get; set; }
        public DateTime DateUpdated { get; set; }
        public DateTime DateCreated { get; set; }
        public string KeyWords { get; set; }
        public string AssetFilePath { get; set; }
        public string AssetLink { get; set; }

        public int MyProperty { get; set; }

        public virtual AssetCategory AssetCategory { get; set; }
        public virtual AssetFileType AssetFileType { get; set; }
        public virtual AssetType AssetType { get; set; }

        [NotMapped]
        public string[] KeyWordsArray
        {
            get
            {
                return this.KeyWords.Split(new char[] { ',' });
            }
        }
    }
}
