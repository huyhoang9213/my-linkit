﻿using System.Collections.Generic;

namespace Linkit.Domain
{
    public partial class AssetFileType
    {
        public AssetFileType()
        {
            Asset = new HashSet<Asset>();
        }

        public int AssetFileTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ThumbnailPath { get; set; }
        public string Extensions { get; set; }

        public virtual ICollection<Asset> Asset { get; set; }
    }
}