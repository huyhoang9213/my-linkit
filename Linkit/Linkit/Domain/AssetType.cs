﻿using System.Collections.Generic;

namespace Linkit.Domain
{
    public partial class AssetType
    {
        public AssetType()
        {
            Asset = new HashSet<Asset>();
        }

        public int AssetTypeId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public string DisplayText { get; set; }
        public int? SortOrder { get; set; }

        public virtual ICollection<Asset> Asset { get; set; }
    }
}