﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Linkit.Migrations
{
    public partial class Inital : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AssetCategories",
                columns: table => new
                {
                    AssetCategoryId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(maxLength: 100, nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    DisplayText = table.Column<string>(maxLength: 100, nullable: true),
                    SortOrder = table.Column<int>(nullable: true, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetCategories", x => x.AssetCategoryId);
                });

            migrationBuilder.CreateTable(
                name: "AssetFileTypes",
                columns: table => new
                {
                    AssetFileTypeId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    Extensions = table.Column<string>(maxLength: 500, nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    ThumbnailPath = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetFileTypes", x => x.AssetFileTypeId);
                });

            migrationBuilder.CreateTable(
                name: "AssetTypes",
                columns: table => new
                {
                    AssetTypeID = table.Column<int>(nullable: false),
                    Code = table.Column<string>(maxLength: 100, nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    DisplayText = table.Column<string>(maxLength: 100, nullable: true),
                    ImagePath = table.Column<string>(maxLength: 500, nullable: true),
                    SortOrder = table.Column<int>(nullable: true, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetTypes", x => x.AssetTypeID);
                });

            migrationBuilder.CreateTable(
                name: "Assets",
                columns: table => new
                {
                    AssetID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AssetCategoryID = table.Column<int>(nullable: true),
                    AssetFilePath = table.Column<string>(maxLength: 500, nullable: true),
                    AssetFileTypeID = table.Column<int>(nullable: true),
                    AssetLink = table.Column<string>(maxLength: 500, nullable: true),
                    AssetTypeID = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(type: "datetime", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "datetime", nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    KeyWords = table.Column<string>(type: "varchar(4000)", nullable: false),
                    Topic = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Assets", x => x.AssetID);
                    table.ForeignKey(
                        name: "FK_Asset_AssetCategory",
                        column: x => x.AssetCategoryID,
                        principalTable: "AssetCategories",
                        principalColumn: "AssetCategoryId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Asset_AssetType",
                        column: x => x.AssetFileTypeID,
                        principalTable: "AssetFileTypes",
                        principalColumn: "AssetFileTypeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Asset_AssetFileType",
                        column: x => x.AssetTypeID,
                        principalTable: "AssetTypes",
                        principalColumn: "AssetTypeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "idx_AssetCategoryID",
                table: "Assets",
                column: "AssetCategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_Assets_AssetFileTypeID",
                table: "Assets",
                column: "AssetFileTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Assets_AssetTypeID",
                table: "Assets",
                column: "AssetTypeID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Assets");

            migrationBuilder.DropTable(
                name: "AssetCategories");

            migrationBuilder.DropTable(
                name: "AssetFileTypes");

            migrationBuilder.DropTable(
                name: "AssetTypes");
        }
    }
}
