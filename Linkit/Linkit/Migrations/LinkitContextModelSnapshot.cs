﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Linkit.Data;

namespace Linkit.Migrations
{
    [DbContext(typeof(LinkitContext))]
    partial class LinkitContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Linkit.Domain.Asset", b =>
                {
                    b.Property<int>("AssetId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("AssetID");

                    b.Property<int?>("AssetCategoryId")
                        .HasColumnName("AssetCategoryID");

                    b.Property<string>("AssetFilePath")
                        .HasMaxLength(500);

                    b.Property<int?>("AssetFileTypeId")
                        .HasColumnName("AssetFileTypeID");

                    b.Property<string>("AssetLink")
                        .HasMaxLength(500);

                    b.Property<int?>("AssetTypeId")
                        .HasColumnName("AssetTypeID");

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("datetime");

                    b.Property<DateTime>("DateUpdated")
                        .HasColumnType("datetime");

                    b.Property<string>("Description")
                        .HasMaxLength(500);

                    b.Property<string>("KeyWords")
                        .IsRequired()
                        .HasColumnType("varchar(4000)");

                    b.Property<int>("MyProperty");

                    b.Property<string>("Topic")
                        .HasMaxLength(100);

                    b.HasKey("AssetId");

                    b.HasIndex("AssetCategoryId")
                        .HasName("idx_AssetCategoryID");

                    b.HasIndex("AssetFileTypeId");

                    b.HasIndex("AssetTypeId");

                    b.ToTable("Assets");
                });

            modelBuilder.Entity("Linkit.Domain.AssetCategory", b =>
                {
                    b.Property<int>("AssetCategoryId")
                        .HasColumnName("AssetCategoryId");

                    b.Property<string>("Code")
                        .HasMaxLength(100);

                    b.Property<string>("Description")
                        .HasMaxLength(500);

                    b.Property<string>("DisplayText")
                        .HasMaxLength(100);

                    b.Property<int?>("SortOrder")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("0");

                    b.HasKey("AssetCategoryId");

                    b.ToTable("AssetCategories");
                });

            modelBuilder.Entity("Linkit.Domain.AssetFileType", b =>
                {
                    b.Property<int>("AssetFileTypeId")
                        .HasColumnName("AssetFileTypeId");

                    b.Property<string>("Description")
                        .HasMaxLength(500);

                    b.Property<string>("Extensions")
                        .HasMaxLength(500);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("ThumbnailPath")
                        .HasMaxLength(500);

                    b.HasKey("AssetFileTypeId");

                    b.ToTable("AssetFileTypes");
                });

            modelBuilder.Entity("Linkit.Domain.AssetType", b =>
                {
                    b.Property<int>("AssetTypeId")
                        .HasColumnName("AssetTypeID");

                    b.Property<string>("Code")
                        .HasMaxLength(100);

                    b.Property<string>("Description")
                        .HasMaxLength(500);

                    b.Property<string>("DisplayText")
                        .HasMaxLength(100);

                    b.Property<string>("ImagePath")
                        .HasMaxLength(500);

                    b.Property<int?>("SortOrder")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("0");

                    b.HasKey("AssetTypeId");

                    b.ToTable("AssetTypes");
                });

            modelBuilder.Entity("Linkit.Domain.Asset", b =>
                {
                    b.HasOne("Linkit.Domain.AssetCategory", "AssetCategory")
                        .WithMany("Asset")
                        .HasForeignKey("AssetCategoryId")
                        .HasConstraintName("FK_Asset_AssetCategory");

                    b.HasOne("Linkit.Domain.AssetFileType", "AssetFileType")
                        .WithMany("Asset")
                        .HasForeignKey("AssetFileTypeId")
                        .HasConstraintName("FK_Asset_AssetType");

                    b.HasOne("Linkit.Domain.AssetType", "AssetType")
                        .WithMany("Asset")
                        .HasForeignKey("AssetTypeId")
                        .HasConstraintName("FK_Asset_AssetFileType");
                });
        }
    }
}
