﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace Linkit.Validations
{
    public class RequiredIfAttribute : ValidationAttribute, IClientModelValidator
    {
        private String PropertyName { get; set; }
        private Object DesiredValue { get; set; }
        private readonly RequiredAttribute _innerAttribute;

        public RequiredIfAttribute(String propertyName, Object desiredvalue)
        {
            PropertyName = propertyName;
            DesiredValue = desiredvalue;
            _innerAttribute = new RequiredAttribute();
        }

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            var dependentValue = context.ObjectInstance.GetType().GetProperty(PropertyName).GetValue(context.ObjectInstance, null);

            if (dependentValue.ToString() == DesiredValue.ToString())
            {
                if (!_innerAttribute.IsValid(value))
                {
                    return new ValidationResult(FormatErrorMessage(context.DisplayName), new[] { context.MemberName });
                }
            }
            return ValidationResult.Success;
        }

        public void AddValidation(ClientModelValidationContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            //var rule = new ModelClientValidationRule
            //{
            //    ErrorMessage = ErrorMessageString,
            //    ValidationType = "requiredif",
            //};
            //rule.ValidationParameters["dependentproperty"] = (context as ViewContext).ViewData.TemplateInfo.GetFullHtmlFieldId(PropertyName);
            //rule.ValidationParameters["desiredvalue"] = DesiredValue is bool ? DesiredValue.ToString().ToLower() : DesiredValue;

            MergeAttribute(context.Attributes, "data-val", "true");
            MergeAttribute(context.Attributes, "data-val-requiredif", ErrorMessageString);

            //var a = (context as ViewContext);
            MergeAttribute(context.Attributes, "data-val-requiredif-dependentproperty", PropertyName);
            MergeAttribute(context.Attributes, "data-val-requiredif-desiredvalue", (DesiredValue.ToString()));
        }
        public static bool MergeAttribute(IDictionary<string, string> attributes, string key, string value)
        {
            if (attributes.ContainsKey(key)) return false;
            attributes.Add(key, value); return true;
        }
    }
    public class DateGreaterThanAttribute : ValidationAttribute, IClientModelValidator
    {
        protected string _otherPropertyName;

        public DateGreaterThanAttribute(string otherPropertyName, string errorMessage)
            : base(errorMessage)
        {
            this._otherPropertyName = otherPropertyName;
        }

        /// <summary>
        /// Server side validation
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ValidationResult result = ValidationResult.Success;
            // using reflection to get a reference to other date property
            var otherPropertyInfo = validationContext.ObjectType.GetProperty(this._otherPropertyName);

            // let's check otherPropery is of type DateTime as we expect it should be
            if (otherPropertyInfo.PropertyType.Equals(new DateTime().GetType()))
            {
                DateTime dateToValidate = (DateTime)value;
                DateTime otherDate = (DateTime)otherPropertyInfo.GetValue(validationContext.ObjectInstance, null);
                if (dateToValidate.CompareTo(otherDate) < 1)
                {
                    result = new ValidationResult(ErrorMessageString);
                }
            }
            else
            {
                result = new ValidationResult("OtherProperty is not type of DateTime");
            }
            return result;
        }

        /// <summary>
        /// Client side validation
        /// </summary>
        /// <param name="context"></param>
        public void AddValidation(ClientModelValidationContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            #region mvc 3
            //ModelClientValidationRule dateGreaterThanRule = new ModelClientValidationRule();
            //dateGreaterThanRule.ErrorMessage = errorMessage;
            //dateGreaterThanRule.ValidationType = "dategreaterthan"; // This is the name the jQuery adapter will use
            ////"otherpropertyname" is the name of the jQuery parameter for the adapter, must be LOWERCASE!
            //dateGreaterThanRule.ValidationParameters.Add("otherpropertyname", otherPropertyName);
            #endregion
            MergeAttribute(context.Attributes, "data-val", "true");
            MergeAttribute(context.Attributes, "data-val-dategreaterthan", ErrorMessageString);
            MergeAttribute(context.Attributes, "data-val-dategreaterthan-otherpropertyname", _otherPropertyName);
        }

        public static bool MergeAttribute(IDictionary<string, string> attributes, string key, string value)
        {
            if (attributes.ContainsKey(key)) return false;
            attributes.Add(key, value); return true;
        }
    }
}
