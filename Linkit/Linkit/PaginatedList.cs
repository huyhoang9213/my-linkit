﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Linkit
{
    public class PaginatedList<T> : List<T>
    {
        public int[] _pageSizes = new int[] { 3, 5, 10, 25, 50, 100 };

        public int CurrentPage { get; private set; }

        public int TotalItems { get; set; }

        public int TotalPages { get; set; }

        public int PageSize { get; set; }

        public SelectList PageSizes
        {
            get
            {
                return new SelectList(_pageSizes, PageSize);
            }
        }

        protected PaginatedList(IEnumerable<T> items, int currentPage, int totalItems, int itemPerPage)
        {
            CurrentPage = currentPage;
            TotalItems = totalItems;
            PageSize = itemPerPage;
            TotalPages = (int)Math.Ceiling(totalItems / (double)itemPerPage);

            this.AddRange(items);
        }

        public bool HasPreviousPage
        {
            get
            {
                return CurrentPage > 1;
            }
        }

        public bool HasNextPage
        {
            get
            {
                return CurrentPage < TotalPages;
            }
        }

        public int FromItem
        {
            get { return ((CurrentPage - 1) * PageSize + 1) > TotalItems ? 0 : ((CurrentPage - 1) * PageSize + 1); }
        }

        public int ToItem
        {
            get { return CurrentPage * PageSize >= TotalItems ? TotalItems : CurrentPage * PageSize; }
        }

        public static async Task<PaginatedList<T>> CreateAsync(IQueryable<T> query, int currentPage, int itemPerPage)
        {
            var items = await query.Skip((currentPage - 1) * itemPerPage).Take(itemPerPage).ToListAsync();
            int count = await query.CountAsync();
            return new PaginatedList<T>(items, currentPage, count, itemPerPage);
        }
    }
}
