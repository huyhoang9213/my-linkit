﻿using Linkit.Domain;
using System;
using System.Linq;

namespace Linkit.Data
{
    public static class SeedData
    {
        public static void InitializeData(LinkitContext context)
        {
            if (context.Assets.Any() || context.AssetCategories.Any()
                || context.AssetFileTypes.Any() 
                || context.AssetTypes.Any())
            {
                return; // DB has been seeded
            }

            //context.Database.Exe .ExecuteSqlCommand
            var assetTypes = new AssetType[]
            {
                new AssetType{ AssetTypeId=1,Code="LT", Description="Laptop",DisplayText="laptop",ImagePath=@"images\",SortOrder=1},
                new AssetType{ AssetTypeId=2,Code="DT", Description="Desktop",DisplayText="desktop",ImagePath=@"images\",SortOrder=2},
                new AssetType{ AssetTypeId=3,Code="Table", Description="Table",DisplayText="table",ImagePath=@"images\",SortOrder=3},
            };
            foreach (var item in assetTypes)
            {
                context.AssetTypes.Add(item);
            }
            context.SaveChanges();

            var assetCategories = new AssetCategory[]
            {
                new AssetCategory{ AssetCategoryId = 1, Code = "Category 1",Description="Category 1 desc", DisplayText="Category 1", SortOrder=1},
                new AssetCategory{ AssetCategoryId = 2, Code = "Category 2",Description="Category 2 desc", DisplayText="Category 2", SortOrder=2},
                new AssetCategory{ AssetCategoryId = 3, Code = "Category 3",Description="Category 3 desc", DisplayText="Category 3", SortOrder=3},
                new AssetCategory{ AssetCategoryId = 4, Code = "Category 4",Description="Category 4 desc", DisplayText="Category 4", SortOrder=4},
                new AssetCategory{ AssetCategoryId = 5, Code = "Category 5",Description="Category 5 desc", DisplayText="Category 5", SortOrder=5},
                new AssetCategory{ AssetCategoryId = 6, Code = "Category 6",Description="Category 6 desc", DisplayText="Category 6", SortOrder=6},
                new AssetCategory{ AssetCategoryId = 7, Code = "Category 7",Description="Category 7 desc", DisplayText="Category 7", SortOrder=7},
                new AssetCategory{ AssetCategoryId = 8, Code = "Category 8",Description="Category 8 desc", DisplayText="Category 8", SortOrder=8},
                new AssetCategory{ AssetCategoryId = 9, Code = "Category 9",Description="Category 9 desc", DisplayText="Category 9", SortOrder=9},
                new AssetCategory{ AssetCategoryId = 10, Code = "Category 10",Description="Category 10 desc", DisplayText="Category 10", SortOrder=10}

            };

            foreach (var item in assetCategories)
            {
                context.AssetCategories.Add(item);
            }
            context.SaveChanges();

            var assetFileTypes = new AssetFileType[]
            {
                    new AssetFileType{ AssetFileTypeId = 1, Name = "Image", Description="Image 1 desc",Extensions="png,jpg",ThumbnailPath="images\thumbnai"},
                     new AssetFileType{ AssetFileTypeId = 2, Name = "Word", Description="Word 1 desc",Extensions="doc,docx",ThumbnailPath="images\thumbnai"},
                     new AssetFileType{ AssetFileTypeId = 3, Name = "Excel", Description="Excel 1 desc",Extensions="excel",ThumbnailPath="images\thumbnai"},
                    new AssetFileType{ AssetFileTypeId = 4, Name = "Pdf", Description="Pdf 1 desc",Extensions="pdf",ThumbnailPath="images\thumbnai"},
              };

            foreach (var item in assetFileTypes)
            {
                context.AssetFileTypes.Add(item);
            }
            context.SaveChanges();

            var assets = new Asset[]
            {
                new Asset{ AssetCategoryId = 1,AssetFilePath="data", AssetFileTypeId=1,AssetLink="https://google.com",AssetTypeId=1,DateCreated=DateTime.Now,DateUpdated=DateTime.Now,Description="This is lorem1",KeyWords="keyword, lorem, poeta1",Topic="topic1"},
                new Asset{ AssetCategoryId = 1,AssetFilePath="data", AssetFileTypeId=2,AssetLink="https://google.com",AssetTypeId=2,DateCreated=DateTime.Now,DateUpdated=DateTime.Now,Description="This is lorem2 test",KeyWords="keyword, lorem, poeta2",Topic="topic2"},
                new Asset{ AssetCategoryId = 1,AssetFilePath="data", AssetFileTypeId=3,AssetLink="https://google.com",AssetTypeId=3,DateCreated=DateTime.Now,DateUpdated=DateTime.Now,Description="This is lorem3",KeyWords="keyword, lorem, poeta3",Topic="topic3"},

                new Asset{ AssetCategoryId = 2,AssetFilePath="data", AssetFileTypeId=1,AssetLink="https://google.com",AssetTypeId=3,DateCreated=DateTime.Now,DateUpdated=DateTime.Now,Description="This is lorem4",KeyWords="keyword, lorem, poeta4",Topic="topic4 test"},
                new Asset{ AssetCategoryId = 10,AssetFilePath="data",AssetFileTypeId=2,AssetLink="https://google.com",AssetTypeId=3,DateCreated=DateTime.Now,DateUpdated=DateTime.Now,Description="This is lorem5",KeyWords="keyword, lorem, poeta5, test",Topic="topic5"},
                new Asset{ AssetCategoryId = 9,AssetFilePath="data", AssetFileTypeId=2,AssetLink="https://google.com",AssetTypeId=3,DateCreated=DateTime.Now,DateUpdated=DateTime.Now,Description="This is lorem6",KeyWords="keyword, lorem, poeta6",Topic="topic6"},

                new Asset{ AssetCategoryId = 3,AssetFilePath="data", AssetFileTypeId=1,AssetLink="https://google.com",AssetTypeId=1,DateCreated=DateTime.Now,DateUpdated=DateTime.Now,Description="This is lorem7",KeyWords="keyword, lorem, poeta7",Topic="topic7"},
                new Asset{ AssetCategoryId = 4,AssetFilePath="data", AssetFileTypeId=2,AssetLink="https://google.com",AssetTypeId=2,DateCreated=DateTime.Now,DateUpdated=DateTime.Now,Description="This is lorem8",KeyWords="keyword, lorem, poeta8",Topic="topic8"},
                new Asset{ AssetCategoryId = 5,AssetFilePath="data", AssetFileTypeId=3,AssetLink="https://google.com",AssetTypeId=3,DateCreated=DateTime.Now,DateUpdated=DateTime.Now,Description="This is lorem9",KeyWords="keyword, lorem, poeta9, test",Topic="topic9"},

                new Asset{ AssetCategoryId = 6,AssetFilePath="data", AssetFileTypeId=1,AssetLink="https://google.com",AssetTypeId=3,DateCreated=DateTime.Now,DateUpdated=DateTime.Now,Description="This is lorem10",KeyWords="keyword, lorem, poeta10",Topic="topic10"},
                new Asset{ AssetCategoryId = 7,AssetFilePath="data", AssetFileTypeId=2,AssetLink="https://google.com",AssetTypeId=3,DateCreated=DateTime.Now,DateUpdated=DateTime.Now,Description="This is lorem11",KeyWords="keyword, lorem, poeta11",Topic="topic11"},
                new Asset{ AssetCategoryId = 8,AssetFilePath="data", AssetFileTypeId=2,AssetLink="https://google.com",AssetTypeId=3,DateCreated=DateTime.Now,DateUpdated=DateTime.Now,Description="This is lorem12",KeyWords="keyword, lorem, poeta12",Topic="topic12"}

        };
            foreach (var item in assets)
            {
                context.Assets.Add(item);
            }
            context.SaveChanges();
        }
    }
}
