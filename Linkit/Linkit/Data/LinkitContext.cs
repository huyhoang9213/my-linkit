﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Linkit.Domain;

namespace Linkit.Data
{
    public partial class LinkitContext : DbContext
    {
        public virtual DbSet<Asset> Assets { get; set; }
        public virtual DbSet<AssetCategory> AssetCategories { get; set; }
        public virtual DbSet<AssetFileType> AssetFileTypes { get; set; }
        public virtual DbSet<AssetType> AssetTypes { get; set; }

        public LinkitContext(DbContextOptions<LinkitContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Asset
            modelBuilder.Entity<Asset>(entity =>
            {
                entity.HasIndex(e => e.AssetCategoryId)
                    .HasName("idx_AssetCategoryID");

                entity.Property(e => e.AssetId).HasColumnName("AssetID");

                entity.Property(e => e.AssetCategoryId).HasColumnName("AssetCategoryID");

                entity.Property(e => e.AssetFilePath).HasMaxLength(500);

                entity.Property(e => e.AssetFileTypeId).HasColumnName("AssetFileTypeID");

                entity.Property(e => e.AssetLink).HasMaxLength(500);

                entity.Property(e => e.AssetTypeId).HasColumnName("AssetTypeID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.DateUpdated).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.KeyWords)
                    .IsRequired()
                    .HasColumnType("varchar(4000)");

                entity.Property(e => e.Topic).HasMaxLength(100);

                entity.HasOne(d => d.AssetCategory)
                    .WithMany(p => p.Asset)
                    .HasForeignKey(d => d.AssetCategoryId)
                    .HasConstraintName("FK_Asset_AssetCategory");

                entity.HasOne(d => d.AssetType)
                    .WithMany(p => p.Asset)
                    .HasForeignKey(d => d.AssetTypeId)
                    .HasConstraintName("FK_Asset_AssetFileType");

                entity.HasOne(d => d.AssetFileType)
                    .WithMany(p => p.Asset)
                    .HasForeignKey(d => d.AssetFileTypeId)
                     .HasConstraintName("FK_Asset_AssetType");
            });

            // AssetCategory
            modelBuilder.Entity<AssetCategory>(entity =>
            {
                entity.HasKey(c => c.AssetCategoryId);

                entity.Property(c => c.AssetCategoryId)
                    .HasColumnName("AssetCategoryId")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code).HasMaxLength(100);

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.DisplayText).HasMaxLength(100);

                entity.Property(e => e.SortOrder).HasDefaultValueSql("0");
            });

            modelBuilder.Entity<AssetFileType>(entity =>
            {
                entity.HasKey(f => f.AssetFileTypeId);
                entity.Property(f => f.AssetFileTypeId)
                    .HasColumnName("AssetFileTypeId")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Extensions).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ThumbnailPath).HasMaxLength(500);
            });

            modelBuilder.Entity<AssetType>(entity =>
            {
                entity.Property(e => e.AssetTypeId)
                    .HasColumnName("AssetTypeID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code).HasMaxLength(100);

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.DisplayText).HasMaxLength(100);

                entity.Property(e => e.ImagePath).HasMaxLength(500);

                entity.Property(e => e.SortOrder).HasDefaultValueSql("0");
            });
        }
    }
}
