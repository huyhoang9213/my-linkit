USE [PoetaCodeFirst]
GO
/****** Object:  Table [dbo].[AssetCategories]    Script Date: 4/12/2017 10:16:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssetCategories](
	[AssetCategoryID] [int] NOT NULL,
	[Code] [nvarchar](100) NULL,
	[Description] [nvarchar](500) NULL,
	[DisplayText] [nvarchar](100) NULL,
	[SortOrder] [int] NULL,
 CONSTRAINT [PK_AssetCategories] PRIMARY KEY CLUSTERED 
(
	[AssetCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AssetFileTypes]    Script Date: 4/12/2017 10:16:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssetFileTypes](
	[AssetFileTypeID] [int] NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Extensions] [nvarchar](500) NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ThumbnailPath] [nvarchar](500) NULL,
 CONSTRAINT [PK_AssetFileTypes] PRIMARY KEY CLUSTERED 
(
	[AssetFileTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Assets]    Script Date: 4/12/2017 10:16:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Assets](
	[AssetID] [int] IDENTITY(1,1) NOT NULL,
	[AssetCategoryID] [int] NULL,
	[AssetFilePath] [nvarchar](500) NULL,
	[AssetFileTypeID] [int] NULL,
	[AssetLink] [nvarchar](500) NULL,
	[AssetTypeID] [int] NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
	[Description] [nvarchar](500) NULL,
	[KeyWords] [varchar](4000) NOT NULL,
	[Topic] [nvarchar](100) NULL,
 CONSTRAINT [PK_Assets] PRIMARY KEY CLUSTERED 
(
	[AssetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AssetTypes]    Script Date: 4/12/2017 10:16:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssetTypes](
	[AssetTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](100) NULL,
	[Description] [nvarchar](500) NULL,
	[DisplayText] [nvarchar](100) NULL,
	[ImagePath] [nvarchar](500) NULL,
	[SortOrder] [int] NULL,
 CONSTRAINT [PK_AssetTypes] PRIMARY KEY CLUSTERED 
(
	[AssetTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[AssetCategories] ADD  DEFAULT ((0)) FOR [SortOrder]
GO
ALTER TABLE [dbo].[AssetTypes] ADD  DEFAULT ((0)) FOR [SortOrder]
GO
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [FK_Asset_AssetCategory] FOREIGN KEY([AssetCategoryID])
REFERENCES [dbo].[AssetCategories] ([AssetCategoryID])
GO
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [FK_Asset_AssetCategory]
GO
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [FK_Asset_AssetFileType] FOREIGN KEY([AssetFileTypeID])
REFERENCES [dbo].[AssetFileTypes] ([AssetFileTypeID])
GO
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [FK_Asset_AssetFileType]
GO
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [FK_Asset_AssetType] FOREIGN KEY([AssetTypeID])
REFERENCES [dbo].[AssetTypes] ([AssetTypeID])
GO
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [FK_Asset_AssetType]
GO
