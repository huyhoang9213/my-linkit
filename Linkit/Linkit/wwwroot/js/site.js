﻿// Write your Javascript code.
(function ($) {
    $.fn.serializeObject2 = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);

$(function () {
    $.validator.unobtrusive.adapters.add('requiredif', ['dependentproperty', 'desiredvalue'],
        function (options) {
            options.rules['requiredif'] = options.params;
            options.messages['requiredif'] = options.message;
        });

    $.validator.addMethod('requiredif', function (value, element, parameters) {
        var desiredvalue = parameters.desiredvalue;
        desiredvalue = (desiredvalue == null ? '' : desiredvalue).toString();
        var controlType = $("input[id$='" + parameters.dependentproperty + "']").attr("type");
        var actualvalue = {}
        if (controlType == "checkbox" || controlType == "radio") {
            var control = $("input[id$='" + parameters.dependentproperty + "']:checked");
            actualvalue = control.val();
        } else {
            actualvalue = $("#" + parameters.dependentproperty).val();
        }
        if ($.trim(desiredvalue).toLowerCase() === $.trim(actualvalue).toLocaleLowerCase()) {
            var isValid = $.validator.methods.required.call(this, value, element, parameters);
            return isValid;
        }
        return true;
    });
}(jQuery));

(function ($) {
    /* File Created: January 16, 2012 */

    // Value is the element to be validated, 
    // params is the array of name/ value pairs of the parameters extracted from the HTML,
    // element is the HTML element that the validator is attached to
    $.validator.addMethod("dategreaterthan", function (value, element, params) {
        console.log("value:" + value);
        console.log("element:" + element);
        console.log("params:" + params);
        return Date.parse(value) > Date.parse($(params).val());
    });

    /* The adapter signature:
    adapterName is the name of the adapter, and matches the name of the rule in the HTML element.
     
    params is an array of parameter names that you're expecting in the HTML attributes,
    and is optional. If it is not provided,
    then it is presumed that the validator has no parameters.
     
    fn is a function which is called to adapt the HTML attribute values into jQuery Validate rules
    and messages.
     
    The function will receive a single parameter which is an options object with the following values in it:
    element
    The HTML element that the validator is attached to
     
    form
    The HTML form element
     
    message
    The message string extract from the HTML attribute
     
    params
    The array of name/value pairs of the parameters extracted from the HTML attributes
     
    rules
    The jQuery rules array for this HTML element. The adapter is expected to add item(s) to this rules array for the specific jQuery Validate validators
    that it wants to attach. The name is the name of the jQuery Validate rule, and the value is the parameter values for the jQuery Validate rule.
     
    messages
    The jQuery messages array for this HTML element. The adapter is expected to add item(s) to this messages array for the specific jQuery Validate validators that it wants to attach, if it wants a custom error message for this rule. The name is the name of the jQuery Validate rule, and the value is the custom message to be displayed when the rule is violated.
    */
    $.validator.unobtrusive.adapters.add("dategreaterthan",
        ["otherpropertyname"],
        function (options) {
            options.rules["dategreaterthan"] = "#" + options.params.otherpropertyname;
            options.messages["dategreaterthan"] = options.message;
            console.log("messages: " + options.message);
        });
})(jQuery);