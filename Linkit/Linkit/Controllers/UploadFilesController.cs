using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Net.Http.Headers;

namespace Linkit.Controllers
{
    public class UploadFilesController : Controller
    {
        private IHostingEnvironment hostingEnv;
        public UploadFilesController(IHostingEnvironment env)
        {
            hostingEnv = env;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> UploadFiles(List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);

            string fileName = "";

            foreach (var formFile in files)
            {
                fileName = ContentDispositionHeaderValue
                               .Parse(formFile.ContentDisposition)
                               .FileName
                               .Trim('"');
                fileName = Path.Combine(hostingEnv.WebRootPath, "upload") + $@"\{fileName}";

                if (formFile.Length > 0)
                {
                    using (var stream = new FileStream(fileName, FileMode.Create))
                    {
                        await formFile.CopyToAsync(stream);
                    }
                }
            }

            // process uploaded files
            // Don't rely on or trust the FileName property without validation

            return Ok(new { count = files.Count });
        }

    }
}