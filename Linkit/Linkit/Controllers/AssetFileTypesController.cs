using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Linkit.Data;
using Linkit.Domain;

namespace Linkit.Controllers
{
    public class AssetFileTypesController : Controller
    {
        private readonly LinkitContext _context;

        public AssetFileTypesController(LinkitContext context)
        {
            _context = context;    
        }

        // GET: AssetFileTypes
        public async Task<IActionResult> Index()
        {
            return View(await _context.AssetFileTypes.ToListAsync());
        }

        // GET: AssetFileTypes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var assetFileType = await _context.AssetFileTypes
                .SingleOrDefaultAsync(m => m.AssetFileTypeId == id);
            if (assetFileType == null)
            {
                return NotFound();
            }

            return View(assetFileType);
        }

        // GET: AssetFileTypes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: AssetFileTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AssetFileTypeId,Name,Description,ThumbnailPath,Extensions")] AssetFileType assetFileType)
        {
            if (ModelState.IsValid)
            {
                _context.Add(assetFileType);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(assetFileType);
        }

        // GET: AssetFileTypes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var assetFileType = await _context.AssetFileTypes.SingleOrDefaultAsync(m => m.AssetFileTypeId == id);
            if (assetFileType == null)
            {
                return NotFound();
            }
            return View(assetFileType);
        }

        // POST: AssetFileTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AssetFileTypeId,Name,Description,ThumbnailPath,Extensions")] AssetFileType assetFileType)
        {
            if (id != assetFileType.AssetFileTypeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(assetFileType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AssetFileTypeExists(assetFileType.AssetFileTypeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(assetFileType);
        }

        // GET: AssetFileTypes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var assetFileType = await _context.AssetFileTypes
                .SingleOrDefaultAsync(m => m.AssetFileTypeId == id);
            if (assetFileType == null)
            {
                return NotFound();
            }

            return View(assetFileType);
        }

        // POST: AssetFileTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var assetFileType = await _context.AssetFileTypes.SingleOrDefaultAsync(m => m.AssetFileTypeId == id);
            _context.AssetFileTypes.Remove(assetFileType);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool AssetFileTypeExists(int id)
        {
            return _context.AssetFileTypes.Any(e => e.AssetFileTypeId == id);
        }
    }
}
