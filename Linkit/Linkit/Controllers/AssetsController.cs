using Linkit.Data;
using Linkit.Domain;
using Linkit.ViewModel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Linkit.Controllers
{
    public class AssetsController : Controller
    {
        private readonly LinkitContext _context;
        private readonly IHostingEnvironment _environment;
        public AssetsController(LinkitContext context, IHostingEnvironment env)
        {
            _context = context;
            _environment = env;
        }

        private List<SelectListItem> PopulateAssetCategories(string[] selectedCategories = null)
        {
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            var dbCategories = _context.AssetCategories.ToList();
            foreach (var item in dbCategories)
            {
                bool selected = selectedCategories == null
                                ? true
                                : selectedCategories.Contains(item.AssetCategoryId.ToString());

                selectListItems.Add(new SelectListItem
                {
                    Selected = selected,
                    Text = item.Code,
                    Value = item.AssetCategoryId.ToString()
                });
            }
            //ViewBag.Categories = selectListItems;
            return selectListItems;

            //new SelectList(_context.AssetCategories.ToList(), "AssetCategoryId", "Code",);
        }

        // GET: Assets
        public IActionResult Index()
        {
            //var linkitContext = _context.Assets.Include(a => a.AssetCategory).Include(a => a.AssetFileType).Include(a => a.AssetType);
            ViewBag.Categories = PopulateAssetCategories();
            return View();
        }

        public async Task<PartialViewResult> List(string[] selectedCategories, string searchString, int? page, int? pageSize)
        {
            var dbAssets = _context.Assets
               .Include(a => a.AssetCategory)
               .Include(a => a.AssetFileType)
               .Include(a => a.AssetType).AsQueryable();

            if (!String.IsNullOrEmpty(searchString))
            {
                dbAssets = dbAssets.Where(c => c.Description.Contains(searchString)
                                     || c.Topic.Contains(searchString)
                                     || c.KeyWords.Contains(searchString));
            }

            if (selectedCategories != null)
            {
                dbAssets = dbAssets.Where(c => c.AssetCategoryId.HasValue == false
                                            || selectedCategories.Contains(c.AssetCategoryId.Value.ToString()));
            }

            PopulateAssetCategories();
            page = page ?? 1;
            pageSize = pageSize ?? 3;
            var assets = await PaginatedList<Asset>.CreateAsync(dbAssets, page.Value, pageSize.Value);

            return PartialView("_List", assets);
        }

        public List<string> GetAllowFileTypes()
        {
            var result = new List<string>();
            string[] fileTypes = _context.AssetFileTypes.Select(c => c.Extensions).ToArray();
            foreach (var item in fileTypes)
            {
                result.AddRange(item.Split(','));
            }
            return result;
        }

        private bool IsValidFileType(string fileType)
        {
            return GetAllowFileTypes().Contains(fileType);
        }


        public IActionResult HelpResourceUpload(int? id)
        {
            ResourceViewModel viewModel = new ResourceViewModel()
            {
                AvailiableCategories = new SelectList(_context.AssetCategories, "AssetCategoryId", "Code"),
                RadioHelpResourceLinkOrFile = "link"
            };

            viewModel.AllowFileTypes = GetAllowFileTypes();

            if (id.HasValue)
            {
                var asset = _context.Assets.Include(c => c.AssetCategory)
                                    .Include(c => c.AssetType)
                                    .Include(c => c.AssetFileType)
                                    .FirstOrDefault(c => c.AssetId == id.Value);
                viewModel.AssetCategoryId = asset.AssetCategoryId;
                viewModel.AssetFilePath = asset.AssetFilePath;
                viewModel.AssetId = asset.AssetId;
                viewModel.AssetLink = asset.AssetLink;
                viewModel.Description = asset.Description;
                viewModel.KeyWords = asset.KeyWords;
                viewModel.Topic = asset.Topic;

                viewModel.AvailiableCategories = new SelectList(_context.AssetCategories, "AssetCategoryId", "Code", asset.AssetCategory);
            }

            viewModel.RadioHelpResourceLinkOrFile = string.IsNullOrEmpty(viewModel.AssetFilePath) ? "link" : "file";
            return View(viewModel);
        }


        [HttpPost]
        public async Task<IActionResult> HelpResourceUpload(ResourceViewModel viewModel, IFormFile AssetFiles)
        {
            #region upload file
            string filePath = "";
            if (viewModel.RadioHelpResourceLinkOrFile == "file")
            {
                filePath = ContentDispositionHeaderValue.Parse(AssetFiles.ContentDisposition).FileName.Trim('"');
                filePath = Path.Combine(_environment.WebRootPath, "upload") + $@"\{filePath}";
                string extention = Path.GetExtension(filePath).Remove(0,1).ToLower();
                if (IsValidFileType(extention) == false)
                {
                    ModelState.AddModelError("", "Forbidden file type");
                    ViewBag.UploadError = "Forbidden file type";
                    return View(viewModel);
                }

                if (AssetFiles.Length > 0)
                {
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await AssetFiles.CopyToAsync(stream);
                    }
                }
                viewModel.AssetFilePath = filePath;
                viewModel.AssetFiles = "valid";
            }
            #endregion

            if (TryValidateModel(viewModel))
            {
                CreateOrUpdateAsset(viewModel);
                return RedirectToAction("Index");
            }

            viewModel.AvailiableCategories = new SelectList(_context.AssetCategories, "AssetCategoryId", "Code");
            viewModel.RadioHelpResourceLinkOrFile = "link";
            return View(viewModel);
        }

        private void CreateOrUpdateAsset(ResourceViewModel viewModel)
        {
            if (viewModel.AssetId == 0)
            {
                Asset asset = new Asset()
                {
                    AssetCategoryId = viewModel.AssetCategoryId,
                    Topic = viewModel.Topic,
                    Description = viewModel.Description,
                    KeyWords = viewModel.KeyWords,
                    AssetLink = viewModel.AssetLink,
                    DateCreated = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    AssetFilePath = viewModel.AssetFilePath
                };
                _context.Assets.Add(asset);
            }
            else
            {
                var dbAsset = _context.Assets.Include(c => c.AssetCategory)
                             .Include(c => c.AssetType)
                             .Include(c => c.AssetFileType)
                             .FirstOrDefault(c => c.AssetId == viewModel.AssetId);
                dbAsset.AssetCategoryId = viewModel.AssetCategoryId;
                dbAsset.AssetFilePath = viewModel.AssetFilePath;
                dbAsset.AssetId = viewModel.AssetId;
                dbAsset.AssetLink = viewModel.AssetLink;
                dbAsset.Description = viewModel.Description;
                dbAsset.KeyWords = viewModel.KeyWords;
                dbAsset.Topic = viewModel.Topic;
                dbAsset.AssetFilePath = viewModel.AssetFilePath;
            }
            _context.SaveChanges();
        }

        // GET: Assets/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var asset = await _context.Assets
                .Include(a => a.AssetCategory)
                .Include(a => a.AssetFileType)
                .Include(a => a.AssetType)
                .SingleOrDefaultAsync(m => m.AssetId == id);
            if (asset == null)
            {
                return NotFound();
            }

            return View(asset);
        }

        // GET: Assets/Create
        public IActionResult Create()
        {
            ViewData["AssetCategoryId"] = new SelectList(_context.AssetCategories, "AssetCategoryId", "AssetCategoryId");
            ViewData["AssetFileTypeId"] = new SelectList(_context.AssetFileTypes, "AssetFileTypeId", "Name");
            ViewData["AssetTypeId"] = new SelectList(_context.AssetTypes, "AssetTypeId", "AssetTypeId");
            return View();
        }

        // POST: Assets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AssetId,AssetTypeId,AssetCategoryId,AssetFileTypeId,Topic,Description,DateUpdated,DateCreated,KeyWords,AssetFilePath,AssetLink")] Asset asset)
        {
            if (ModelState.IsValid)
            {
                _context.Add(asset);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["AssetCategoryId"] = new SelectList(_context.AssetCategories, "AssetCategoryId", "AssetCategoryId", asset.AssetCategoryId);
            ViewData["AssetFileTypeId"] = new SelectList(_context.AssetFileTypes, "AssetFileTypeId", "Name", asset.AssetFileTypeId);
            ViewData["AssetTypeId"] = new SelectList(_context.AssetTypes, "AssetTypeId", "AssetTypeId", asset.AssetTypeId);
            return View(asset);
        }

        // GET: Assets/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var asset = await _context.Assets.SingleOrDefaultAsync(m => m.AssetId == id);
            if (asset == null)
            {
                return NotFound();
            }
            ViewData["AssetCategoryId"] = new SelectList(_context.AssetCategories, "AssetCategoryId", "AssetCategoryId", asset.AssetCategoryId);
            ViewData["AssetFileTypeId"] = new SelectList(_context.AssetFileTypes, "AssetFileTypeId", "Name", asset.AssetFileTypeId);
            ViewData["AssetTypeId"] = new SelectList(_context.AssetTypes, "AssetTypeId", "AssetTypeId", asset.AssetTypeId);
            return View(asset);
        }

        // POST: Assets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AssetId,AssetTypeId,AssetCategoryId,AssetFileTypeId,Topic,Description,DateUpdated,DateCreated,KeyWords,AssetFilePath,AssetLink")] Asset asset)
        {
            if (id != asset.AssetId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(asset);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AssetExists(asset.AssetId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["AssetCategoryId"] = new SelectList(_context.AssetCategories, "AssetCategoryId", "AssetCategoryId", asset.AssetCategoryId);
            ViewData["AssetFileTypeId"] = new SelectList(_context.AssetFileTypes, "AssetFileTypeId", "Name", asset.AssetFileTypeId);
            ViewData["AssetTypeId"] = new SelectList(_context.AssetTypes, "AssetTypeId", "AssetTypeId", asset.AssetTypeId);
            return View(asset);
        }

        // GET: Assets/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var asset = await _context.Assets
                .Include(a => a.AssetCategory)
                .Include(a => a.AssetFileType)
                .Include(a => a.AssetType)
                .SingleOrDefaultAsync(m => m.AssetId == id);
            if (asset == null)
            {
                return NotFound();
            }

            return View(asset);
        }

        // POST: Assets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var asset = await _context.Assets.SingleOrDefaultAsync(m => m.AssetId == id);
            _context.Assets.Remove(asset);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool AssetExists(int id)
        {
            return _context.Assets.Any(e => e.AssetId == id);
        }
    }
}
