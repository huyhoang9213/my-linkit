﻿using Linkit.Validations;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Linkit.ViewModel
{
    public class ResourceViewModel
    {
        public ResourceViewModel()
        {
            AllowFileTypes = new List<string>();
        }
        public SelectList AvailiableCategories { get; set; }

        public int AssetId { get; set; }

        [Display(Name = "Category")]
        public int? AssetCategoryId { get; set; }

        [Required]
        public string Topic { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public string KeyWords { get; set; }

        public string RadioHelpResourceLinkOrFile { get; set; }

    
        public string AssetFilePath { get; set; }

        [Display(Name = "File path")]
        [RequiredIf("RadioHelpResourceLinkOrFile", "file", ErrorMessage = "Please upload file")]
        public string AssetFiles { get; set; }

        [Display(Name = "Link path")]
        [DataType(DataType.Url)]
        [RequiredIf("RadioHelpResourceLinkOrFile", "link", ErrorMessage = "Required link...")]
        public string AssetLink { get; set; }

        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [DataType(DataType.Date)]
        [DateGreaterThan("StartDate", "EndDAte must be greater than StartDate", ErrorMessage = "enddate must be greater than startdate")]
        public DateTime EndDate { get; set; }

        public List<string> AllowFileTypes
        {
            get;set;
        }
    }
}
